[Exemple de livrable déposé](https://gitlab.com/incubateur-territoires/france-relance/datami/datami/-/issues/1)

## Ca se passe où ?

Les livrables se déposent un ouvrant un ticket dans le projet.

Avant de pouvoir déposer les fichiers, il faut initier cette "issue".

## Création préalable de la fiche

Titre : `Livrables projet`

Description

```
Accompagnements :
- [ ] 🆓 Open source
- [ ] 💅 Design
- [ ] 📖 Documentation
- [ ] 🌡️ Mesure d'impact
- [ ] ☘️ Eco-conception
- [ ] 🧑‍💻 Dev
```

\-> Enregistrer

Dans la colonne de droite, il y a une icone pour le "label". On l'édite et on coche "Livrable". Ca permet de les trouver plus facilement. Si on ne peut pas éditer, c'est qu'on n'a pas les droits suffisants. Contacter Vincent A. (@vinyll )


 <img width="200" src="uploads/afcfd9c18b67e26ab54047555ed72438/Screenshot_2022-12-19_at_17.46.52.png">

## Dépôt des documents pour une expertise

[Exemple de dépôt de livrable](https://gitlab.com/incubateur-territoires/france-relance/datami/datami/-/issues/1)

Une fois cette fiche préparée, on pourra l'alimenter :

1. on crée une 1e "Activity" en dessous sur une des expertises (ex: Open source),
2. on dépose les documents dans ce commentaire d'activité,
3. on coche en haut dans la description puisque c'est déposé.

Le commentaire doit contenir à minimal l'intitulé de l'expertise, le nom de l'expert, la date (mois/année) de la livraison, et l'ensemble des fichiers livrables pour cette expertise.

```
## Open source

- Auteur : Inno3
- Date : 11/2022

```


On refait l'étape "dépôt des documents pour une expertise", pour chaque expertise.
